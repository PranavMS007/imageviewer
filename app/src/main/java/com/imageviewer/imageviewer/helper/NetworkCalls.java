package com.imageviewer.imageviewer.helper;

import android.content.Context;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NetworkCalls {
    private JSONParser jsonParser;
    Context ctx ;

    /*
    * Constructor
    */
    public NetworkCalls(Context context){
        ctx = context;
        jsonParser = new JSONParser();
    }

    /*
    * Creating the network calls
    */
    public JSONObject getFlickerResponse(String subURL){
        JSONObject json = null;
        try {
            String URL=Constants.BASE_URL+subURL;
            json = jsonParser.getJSONFromUrl(URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("json--",json.toString());
        return json;
    }
}
