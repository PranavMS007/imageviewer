package com.imageviewer.imageviewer.helper;

public class Constants {
    public static final String BASE_URL =  "https://api.flickr.com";
    public static final String IMAGE_GET_URL =  "/services/feeds/photos_public.gne";

    public static final String LIST_GET_URL =  "/services/feeds/photos_public.gne?format=json&nojsoncallback=1";
    public static final String API_FLICKER_ITEMS =  "items";
    public static final String API_FLICKER_TITLE =  "title";
    public static final String API_FLICKER_MEDIA =  "media";
    public static final String API_FLICKER_IMG_LINK =  "m";

}
