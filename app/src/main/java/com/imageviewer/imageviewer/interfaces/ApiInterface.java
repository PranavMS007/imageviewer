package com.imageviewer.imageviewer.interfaces;

import com.imageviewer.imageviewer.helper.Constants;
import com.imageviewer.imageviewer.models.FlickerData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET(Constants.IMAGE_GET_URL)
    Call<FlickerData> fetchFlickerData(@Query("format") String param1, @Query("nojsoncallback") String param2);

}
