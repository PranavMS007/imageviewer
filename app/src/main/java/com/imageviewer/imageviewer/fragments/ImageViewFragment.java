package com.imageviewer.imageviewer.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imageviewer.imageviewer.R;
import com.imageviewer.imageviewer.helper.BitMapHelper;
import com.imageviewer.imageviewer.helper.ConnectionDetector;
import com.imageviewer.imageviewer.helper.ImageDownloaderTask;

public class ImageViewFragment extends Fragment {

    ConnectionDetector connectionDetector;
    BitMapHelper bitMapHelper;

    public static ImageViewFragment newInstance(String tittle,String imageUrl) {
        ImageViewFragment imageViewFragment = new ImageViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tittle", tittle);
        bundle.putString("imageurl", imageUrl);

        imageViewFragment.setArguments(bundle);
        return imageViewFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.image_view_fragment, container, false);

        connectionDetector=new ConnectionDetector(getContext());
        bitMapHelper=new BitMapHelper();
        TextView flickerImageName=(TextView)view.findViewById(R.id.flickerImageName);
        if(getArguments().getString("tittle").trim().isEmpty()){
            flickerImageName.setText("Image Name is not available");
        }else{
            flickerImageName.setText(getArguments().getString("tittle"));
        }

        ImageView flickerImage=(ImageView) view.findViewById(R.id.flickerImage);
        if (connectionDetector.isConnectingToInternet()) {
            Bitmap img = BitMapHelper.getBitmapFromURL(getArguments().getString("imageurl"));
            new ImageDownloaderTask(flickerImage).onPostExecute(img);
        }else{
            flickerImage.setImageResource(R.drawable.flickr_logo);
        }
        return view;
    }
}
