package com.imageviewer.imageviewer.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.imageviewer.imageviewer.R;
import com.imageviewer.imageviewer.adapter.ViewPageAdapter;
import com.imageviewer.imageviewer.helper.ApiClient;
import com.imageviewer.imageviewer.helper.ConnectionDetector;
import com.imageviewer.imageviewer.helper.NetworkCalls;
import com.imageviewer.imageviewer.helper.PermissionsChecker;
import com.imageviewer.imageviewer.interfaces.ApiInterface;
import com.imageviewer.imageviewer.models.FlickerData;
import com.imageviewer.imageviewer.models.FlickerItem;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    PermissionsChecker permissionsChecker;
    ConnectionDetector connectionDetector;
    NetworkCalls networkCalls;
    List<FlickerItem> flickerItems;

    ViewPager viewPager;
    CirclePageIndicator circlePageIndicator;
    SwipeRefreshLayout swipeRefreshLayout;
    ViewPager.SimpleOnPageChangeListener pagerSyncronizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            // checking the permission t access the internet
            permissionsChecker = new PermissionsChecker(this);
            int PERMISSION_ALL = 1;
            String[] PERMISSIONS = {Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_NETWORK_STATE};
            if (!permissionsChecker.hasPermissions(PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }

            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }

            connectionDetector = new ConnectionDetector(this);
            networkCalls = new NetworkCalls(this);

            flickerItems = null;

            // ui elements
            viewPager = findViewById(R.id.flickerViewpager);
            viewPager.setOffscreenPageLimit(5);
            circlePageIndicator = findViewById(R.id.circlePageIndicator);
            pagerSyncronizer = getPagerSynchronizer();

            swipeRefreshLayout =findViewById(R.id.swiperefresh);
            swipeRefreshLayout.setEnabled(true);
            swipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            Log.i("swipeRefreshLayout--", "onRefresh called from SwipeRefreshLayout");
                            // This method performs the actual data-refresh operation.
                            // The method calls setRefreshing(false) when it's finished.
                            if (connectionDetector.isConnectingToInternet()) {
                                try {
                                    swipeRefreshLayout.setRefreshing(true);
                                    getFlickerResponse();
                                    viewPager.getAdapter().notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                showAlertDialog(MainActivity.this);
                            }
                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!connectionDetector.isConnectingToInternet()) {
            showAlertDialog(MainActivity.this);
        } else {
            if (flickerItems == null) {
                try {
                    swipeRefreshLayout.setRefreshing(true);
                    getFlickerResponse();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
     * for sync the viewpage item with page indicator to indicate
     */
    private ViewPager.SimpleOnPageChangeListener getPagerSynchronizer() {
        return new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position, true);
            }
        };
    }

    /*
     * Function to show the alert dialogue
     */
    public void showAlertDialog(Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        // Setting Dialog Title
        alertDialog.setTitle("Alert !..");
        // setting cancellable false
        alertDialog.setCancelable(false);
        // Setting Dialog Message
        alertDialog.setMessage("Please concet to an internet connection");
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //calling the phone settings
                startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void getFlickerResponse(){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<FlickerData> call = apiService.fetchFlickerData("json","1");
        call.enqueue(new Callback<FlickerData>() {
            @Override
            public void onResponse(Call<FlickerData> call, Response<FlickerData> response) {
                try{
                    List<FlickerItem> FlickerDataResponse=new ArrayList<>();
                    FlickerDataResponse= response.body().getItems();
                    flickerItems =FlickerDataResponse;

                    swipeRefreshLayout.setRefreshing(false);
                    viewPager.setAdapter(new ViewPageAdapter(getSupportFragmentManager(), FlickerDataResponse));
                    circlePageIndicator.setViewPager(viewPager);
                    circlePageIndicator.setOnPageChangeListener(pagerSyncronizer);

                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<FlickerData> call, Throwable t) {
                Log.e(this + "-----", t.toString());
            }
        });
    }
}
