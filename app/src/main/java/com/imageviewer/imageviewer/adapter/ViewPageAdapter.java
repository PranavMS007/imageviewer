package com.imageviewer.imageviewer.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.imageviewer.imageviewer.fragments.ImageViewFragment;
import com.imageviewer.imageviewer.models.FlickerItem;

import java.util.List;

public class ViewPageAdapter extends FragmentStatePagerAdapter {
    private List<FlickerItem> adapterData;
    public ViewPageAdapter(FragmentManager fragmentManager,List<FlickerItem> flickerData) {
        super(fragmentManager);
        adapterData =flickerData;
    }

    @Override
    public int getCount() {
        int count= 0;
        try {
            count = adapterData.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public Fragment getItem(int pos) {
        String tittle="",   imageUrl="";
        try {
            tittle= adapterData.get(pos).getTitle();
            imageUrl= adapterData.get(pos).getMedia().getM();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ImageViewFragment.newInstance(tittle,imageUrl);
    }
    //https://stackoverflow.com/questions/7263291/viewpager-pageradapter-not-updating-the-view
//    @Override
//    public int getItemPosition(Object object) {
//        return POSITION_NONE;
//    }
}
